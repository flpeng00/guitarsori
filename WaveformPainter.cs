﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace paDemo
{
    public class WaveformPainter
    {
        static Pen pen;
        static Rectangle[] sampleRects;
        int midY, numX;

        public WaveformPainter(Size s)
        {
            midY = s.Height / 2;
            numX = s.Width;
            Init();
        }

        public void PaintSample(ref float[] samples)
        {
            for (int i = 0; i < 100; i++)
            {
                int y = (int)(samples[i]*10 * (float)midY);
                sampleRects[i].Height = Math.Abs(y);
                if (sampleRects[i].Height < 1)
                    sampleRects[i].Height = 1;
                y = midY + y;
                sampleRects[i].Y = y;
            }
            MainForm.g.DrawRectangles(pen, sampleRects);
        }

        public void Init()
        {
            sampleRects = new Rectangle[numX];
            pen = new Pen(new SolidBrush(Color.LightGreen));
            MainForm.g.Clear(Color.Black);
            for (int i = 0; i < numX; i++)
            {
                sampleRects[i] = new Rectangle(i, midY, 1, 1);
            }
            MainForm.g.DrawRectangles(pen, sampleRects);
        }
    }
}
