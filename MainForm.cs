﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace paDemo
{
    public partial class MainForm : Form
    {
        public static WaveformPainter wfPainter;
        public static AudioController audioCon;
        public static Graphics g;
        private double[] x;
        public static PointPairList list;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            audioCon = new AudioController();
            x = new double[1024];
            GraphPane wvPane = wvControl.GraphPane;
            list = new PointPairList();
            wvPane.AddCurve("wave", list, Color.Black);
            for (int i = 0; i < 1024; i++)
            {
                x[i] = i;
            }
            wvPane.Title.Text = "Waveform (Sample Rate : " + AudioController.SAMPLE_RATE + " Frames : " + AudioController.FRAMES_PER_BUFFER;
            timer1.Interval = 30;
            timer1.Enabled = true;
            list.SetX(x);

            /*
            pb_waveform.Image = new Bitmap(pb_waveform.Width, pb_waveform.Height);
            g = Graphics.FromImage(pb_waveform.Image);
            wfPainter = new WaveformPainter(pb_waveform.Image.Size);
             * */

        }

        public static void plot(double[] samples)
        {
            list.SetY(samples);
        }


        public void initImage()
        {
            /*
            pb_waveform.Image = new Bitmap(pb_waveform.Width, pb_waveform.Height);
            g = Graphics.FromImage(pb_waveform.Image);
            g.Clear(Color.Black);
             */
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            //wvControl.Invalidate();
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            //initImage();
            
            audioCon.run();
            //timer1.Start(); 
        }

    }   
}
