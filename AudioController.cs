﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;
using PortAudioSharp;

namespace paDemo
{
    public class AudioController
    {
        public const int SAMPLE_RATE = 44100;
        public const int FRAMES_PER_BUFFER = 1024;
        static float[] sampleBuffer;
        static float[] ffted;
        PortAudio.PaError err;
        IntPtr stream;

        public AudioController()
        {
            ffted = new float[1024];
            sampleBuffer = new float[FRAMES_PER_BUFFER];
        }

        public static PortAudio.PaStreamCallbackResult inputStreamCallback(
            IntPtr input,
            IntPtr output,
            uint frameCount,
            ref PortAudio.PaStreamCallbackTimeInfo timeInfo,
            PortAudio.PaStreamCallbackFlags statusFlags,
            IntPtr userData)
        {
            Marshal.Copy(input, sampleBuffer, 0, FRAMES_PER_BUFFER);
            MainForm.plot(Array.ConvertAll(sampleBuffer, x => (double)x));
            //MainForm.wfPainter.PaintSample(ref sampleBuffer);
            ffted = Calc.FFT(sampleBuffer);
            return PortAudio.PaStreamCallbackResult.paContinue;
        }

        public void run()
        {
            int i, deviceNum;
            PortAudio.PaStreamParameters streamParameters = new PortAudio.PaStreamParameters();
            PortAudio.PaStreamParameters outputParams = new PortAudio.PaStreamParameters();

            err = PortAudio.Pa_Initialize();
            i = 1;

            deviceNum = PortAudio.Pa_GetDeviceCount();
            PortAudio.PaDeviceInfo deviceInfo = PortAudio.Pa_GetDeviceInfo(i);

            Log.Write("Default Input Device : ");
            Log.Write("Name                 : " + deviceInfo.name);
            Log.Write("Host API             : \n" + PortAudio.Pa_GetHostApiInfo(deviceInfo.hostApi));
            Log.Write("Max Input Channel    : " + deviceInfo.maxInputChannels);
            Log.Write("Max Output Channel   : " + deviceInfo.maxOutputChannels);
            Log.Write("Default Sample Rate  : " + deviceInfo.defaultSampleRate);

            stream = new IntPtr();
            streamParameters.device = i;
            streamParameters.channelCount = deviceInfo.maxInputChannels;
            streamParameters.sampleFormat = PortAudio.PaSampleFormat.paFloat32;
            streamParameters.suggestedLatency = 0;
            outputParams.device = 2;
            outputParams.channelCount = 1;
            outputParams.sampleFormat = PortAudio.PaSampleFormat.paFloat32;

            err = PortAudio.Pa_OpenStream(out stream, ref streamParameters, ref outputParams, SAMPLE_RATE, FRAMES_PER_BUFFER,
                PortAudio.PaStreamFlags.paClipOff, new PortAudio.PaStreamCallbackDelegate(inputStreamCallback), IntPtr.Zero);
            Log.Write("Stream Open");
            err = PortAudio.Pa_StartStream(stream);
            Log.Write("Stream Start");
            //MainForm.g.Clear(Color.Black);

        }

        public void stop()
        {
            err = PortAudio.Pa_CloseStream(stream);
        }
    }
}
