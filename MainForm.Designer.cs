﻿namespace paDemo
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button_start = new System.Windows.Forms.Button();
            this.wvControl = new ZedGraph.ZedGraphControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(12, 12);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(105, 43);
            this.button_start.TabIndex = 0;
            this.button_start.Text = "녹음 시작";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // wvControl
            // 
            this.wvControl.Location = new System.Drawing.Point(13, 62);
            this.wvControl.Name = "wvControl";
            this.wvControl.ScrollGrace = 0D;
            this.wvControl.ScrollMaxX = 0D;
            this.wvControl.ScrollMaxY = 0D;
            this.wvControl.ScrollMaxY2 = 0D;
            this.wvControl.ScrollMinX = 0D;
            this.wvControl.ScrollMinY = 0D;
            this.wvControl.ScrollMinY2 = 0D;
            this.wvControl.Size = new System.Drawing.Size(861, 237);
            this.wvControl.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 591);
            this.Controls.Add(this.wvControl);
            this.Controls.Add(this.button_start);
            this.Name = "MainForm";
            this.Text = "PA_DEMO by flpeng00";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_start;
        private ZedGraph.ZedGraphControl wvControl;
        private System.Windows.Forms.Timer timer1;
    }
}

